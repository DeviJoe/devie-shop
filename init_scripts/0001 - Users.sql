create table users
(
    id                    integer default nextval('table_name_id_seq'::regclass) not null
        constraint table_name_pk
            primary key,
    username              text,
    email                 text                                                   not null,
    password              text,
    "user_role"            text                                                   not null,
    "is_account_not_blocked" text
);

alter table users
    owner to shop;

create unique index table_name_email_uindex
    on users (email);

create unique index table_name_id_uindex
    on users (id);