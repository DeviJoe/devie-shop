create table products
(
    id          serial
        constraint products_pk
            primary key,
    version     integer,
    "product_id" text,
    name        text,
    price       integer,
    "photo_name" text
);

alter table products
    owner to shop;

create unique index products_id_uindex
    on products (id);