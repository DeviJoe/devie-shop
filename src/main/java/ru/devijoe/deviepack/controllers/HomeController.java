package ru.devijoe.deviepack.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;

@RestController
public class HomeController {
    
    @Value("${server.servlet.context-path}")
    private String baseUrl;
    
    @RequestMapping(value = "/default", method = RequestMethod.GET)
    public RedirectView defaultAfterLogin(HttpServletRequest request) {
        return new RedirectView(baseUrl +  "/");
    }
}
