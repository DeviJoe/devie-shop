package ru.devijoe.deviepack.controllers;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import ru.devijoe.deviepack.repositories.ProductRepository;
import ru.devijoe.deviepack.services.impl.FileSystemStorageService;

import java.util.stream.Collectors;

@RestController
@Slf4j
public class ProductsController {

    private ProductRepository productRepository;
    private FileSystemStorageService storageService;

    @Autowired
    public void setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Autowired
    public void setStorageService(FileSystemStorageService storageService) {
        this.storageService = storageService;
    }

    /**
     * Получение списка всех продуктов интернет-магазина
     * @param pageable
     * @param model
     * @return
     */
    @RequestMapping(value = "/products", method = RequestMethod.GET)
    public String list(@PageableDefault(size = 8, direction = Sort.Direction.ASC, sort = "price") Pageable pageable, Model model) {
        model.addAttribute("products", productRepository.findAll(pageable));
        model.addAttribute("files", storageService
                .loadAll()
                .map(path ->
                        MvcUriComponentsBuilder
                                .fromMethodName(ProductsController.class, "serveFile", path.getFileName().toString())
                                .build().toString())
                .collect(Collectors.toList()));
        log.info("All products");
        return new Gson().toJson(model);
    }

    /**
     * Получение конкретного продукта по его ID
     * @param id
     * @param model
     * @return
     */
    @RequestMapping("product/{id}")
    public String showProduct(@PathVariable Long id, Model model) {
        model.addAttribute("product", productRepository.findById(id));
        log.info("Prodused product id: " + id);
        return new Gson().toJson(model);
    }

    /**
     * Удаление продукта из базы по ID
     * @param id
     * @return
     */
    @RequestMapping("product/delete/{id}")
    public String delete(@PathVariable Long id) {
        productRepository.deleteById(id);
        log.info("Deleted product id : " + id);
        return "redirect:/products";
    }

}
