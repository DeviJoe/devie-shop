package ru.devijoe.deviepack.configuration;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import ru.devijoe.deviepack.model.Role;
import ru.devijoe.deviepack.services.impl.DevieUserDetailsService;

@Configuration
@EnableWebSecurity
@Lazy
public class SecureConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private DevieUserDetailsService devieUserDetailsService;


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/product/?","/","/home","/user/**","/registration","/products","/default").permitAll()
                .antMatchers("/cabinet","/principle/**" ).authenticated()
                .antMatchers("/**").hasAuthority(String.valueOf(Role.ADMIN))
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .defaultSuccessUrl("/default")
                .successForwardUrl("/default")
                .permitAll()
                .and()
                .logout()
                .permitAll().deleteCookies("JSESSIONID").and()
                .exceptionHandling().accessDeniedPage("/accessDenied");
    }

    @Override
    public void configure(WebSecurity web) {
        web
                .ignoring()
                .antMatchers("/bower_components/**","/js/**","/css/**","/images/**");
    }


    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider
                = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(devieUserDetailsService);
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
