package ru.devijoe.deviepack.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("storage")
public class StorageConfiguration {

    @Getter
    @Setter
    private String location;

}
