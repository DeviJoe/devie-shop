package ru.devijoe.deviepack.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue
    private Long id;

    @Version
    private Integer version;

    @Column(name = "product_id")
    private String productId;

    private String name;

    private Integer price;

    @Column(name = "photo_name")
    private String photoName;

    public Product(Long id, Integer version, String productId, String name, Integer price, String photoName) {
        this.id = id;
        this.version = version;
        this.productId = productId;
        this.name = name;
        this.price = price;
        this.photoName = photoName;
    }
}
