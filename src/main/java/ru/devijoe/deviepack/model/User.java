package ru.devijoe.deviepack.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.*;

@NoArgsConstructor
@Data
@Entity
@Table(name = "users", schema = "public")
public class User {

    @Id
    @GeneratedValue
    private Long id;

    private String username;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "password")
    private String passwd;

    @Column(name = "user_role", nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role;

    @Column(name = "is_account_not_blocked")
    private boolean isAccountNotBlocked;

    public User(String username, String email, String password, Role role, boolean isAccountNotBlocked) {
        this.username = username;
        this.email = email;
        this.passwd = new BCryptPasswordEncoder().encode(password);
        this.role = role;
        this.isAccountNotBlocked = isAccountNotBlocked;
    }

}
