package ru.devijoe.deviepack.model;

public enum Role {
    USER("Role user"),
    ADMIN("Role admin");

    private final String description;

    Role(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
