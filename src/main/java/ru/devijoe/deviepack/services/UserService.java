package ru.devijoe.deviepack.services;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import ru.devijoe.deviepack.exceptions.EmailExistsException;
import ru.devijoe.deviepack.model.User;
import ru.devijoe.deviepack.repositories.UserRepository;

public interface UserService {

    void setUserRepository(UserRepository userRepository);

    void setbCryptPasswordEncoder(BCryptPasswordEncoder bCryptPasswordEncoder);

    User registerNewAccount(User user) throws EmailExistsException;


}
