package ru.devijoe.deviepack.services.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.devijoe.deviepack.exceptions.EmailExistsException;
import ru.devijoe.deviepack.model.Role;
import ru.devijoe.deviepack.model.User;
import ru.devijoe.deviepack.repositories.UserRepository;
import ru.devijoe.deviepack.services.UserService;

import javax.transaction.Transactional;


@Service
@Slf4j
public class UserServiceImpl implements UserService {

    UserRepository userRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setbCryptPasswordEncoder(BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Transactional
    public User registerNewAccount(User user) throws EmailExistsException {
        if (userRepository.findByEmail(user.getEmail()) != null) {
            throw new EmailExistsException("Email address alrady exists: " + user.getEmail());
        } else {
            user.setUsername(user.getUsername());
            user.setEmail(user.getEmail());
            user.setPasswd(bCryptPasswordEncoder.encode(user.getPasswd()));
            user.setRole(Role.USER);
            user.setAccountNotBlocked(Boolean.TRUE);
            log.info("User" + user.getUsername() + "created");
            return userRepository.save(user);
        }
    }
}
